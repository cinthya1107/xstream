/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import acceso.Cazador;
import acceso.Habilidad;

/**
 *
 * @author CinthyaPérezVazquez
 */
public class Prueba1 {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		Habilidad hab = new Habilidad("Agua", 1, "Agua", 2, 4);
		Habilidad hab2 = new Habilidad("Llama", 2, "Fuego", 2, 4);
		
		Cazador caz = new Cazador();
		Cazador caz2;
		
		XStream x1 = new XStream(new DomDriver());
		String xml = x1.toXML(caz);

		boolean continuar = true;

		while (continuar) {

			System.out.println(

					"Introduzca su eleccion \n 1)Informaci�n del xml \n 2)Guardar datos en el xml \n 3)Borrar datos \n 4)Salir");

			int eleccion = sc.nextInt();

			if (eleccion == 1)

				try {
					Cazador c1 = (Cazador) x1.fromXML(Files.newBufferedReader(Paths.get("cazador.xml")));
					c1.Info();
				} catch (Exception e) {
					e.printStackTrace();
				}

			if (eleccion == 2) {

				try {
					// Habilidad hab = ;
					x1.toXML(caz, Files.newBufferedWriter(Paths.get("cazador.xml")));
					caz.addHabilidad(hab);
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			if (eleccion == 3) {
				try {
					x1.toXML(caz, Files.newBufferedWriter(Paths.get("cazador.xml")));
					caz.removeHabilidad(hab2);
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			if (eleccion == 4) {

				continuar = false;

			}

		}

	}

}
