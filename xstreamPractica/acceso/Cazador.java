/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;

import java.util.ArrayList;

/**
 *
 * @author CinthyaPérezVazquez
 */
public class Cazador {

    private ArrayList<Habilidad> habilidades;

    public Cazador() {
        this.habilidades = new ArrayList<Habilidad>();
    }

    public void Info() {
        System.out.println("Los cazadores tienen: ");
        for (int i = 0; i < habilidades.size(); i++) {
            System.out.println(habilidades.get(i));
        }
    }

    public void addHabilidad(Habilidad hab) {
        this.habilidades.add(hab);
    }

    public void removeHabilidad(Habilidad hab) {
        this.habilidades.remove(hab);
    }

}
