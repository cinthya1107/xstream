/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso;

/**
 *
 * @author CinthyaPérezVazquez
 */
public class Habilidad {

    private String nombre;
    private int ID;
    private String tipo;
    private int cantidad;
    private int nivel;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public Habilidad(String nombre, int ID, String tipo, int cantidad, int nivel) {
        this.nombre = nombre;
        this.ID = ID;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.nivel = nivel;
    }


    @Override
    public String toString() {
       return "Habilidades{" + "\nnombre=" + nombre + ", \nID=" + ID + ", \ntipo=" + tipo + ", \ncantidad=" + cantidad + ", \nnivel=" + nivel + "\n}";
    }

}
